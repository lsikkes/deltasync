﻿using CommandLine;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;

namespace deltasync
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(o =>
                {
                    if (o.Verbose)
                    {
                        Console.Error.WriteLine("Arguments: {0}", JsonConvert.SerializeObject(o,Formatting.Indented).ToString());
                    }

                    Execute(o);
                });
        }


        static async Task<FilesAndDirectories> Gather(Options o, string folder)
        {
            FilesAndDirectories ret = new FilesAndDirectories();

            // load snapshot?
            if(folder.StartsWith("#"))
            {
                ret = JsonConvert.DeserializeObject<FilesAndDirectories>(System.IO.File.ReadAllText(folder.Substring(1)));
                return ret;
            }

            folder = System.IO.Path.GetFullPath(folder);
            Task t = new Task( async () =>
            {
                if (o.Verbose)
                    Console.Error.WriteLine("Gathering " + folder);

                ret = await Helpers.GatherFilesAndDirectories(folder + "\\");

                if (o.Verbose)
                    Console.Error.WriteLine("Hashing " + folder);

                if (o.DateModified)
                    ret.CalculateDateTimeAsHash();
                else
                    ret.CalculateHashes();

                if (o.Verbose)
                    Console.Error.WriteLine("Done with " + folder);
            });

            try { t.Start(); } catch (Exception) { }

            await t;

            return ret;
        }

        static void Execute(Options o)
        {
            if (o.Snapshot == false)
            {
                Task<FilesAndDirectories> t = Gather(o, o.SourceFolder);
                Task<FilesAndDirectories> t2 = Gather(o, o.TargetFolder);
                Task.WaitAll(t, t2);
                GenerateOutput(t.Result, t2.Result);
            }
            else
            {
                Task<FilesAndDirectories> t = Gather(o, o.SourceFolder);
                var data = t.Result;
                Console.WriteLine(JsonConvert.SerializeObject(data, Formatting.Indented));
            }
        }

        static void GenerateOutput(FilesAndDirectories sourceDir, FilesAndDirectories targetDir)
        {
            var e = new FilesAndDirectories();
            var a = new FilesAndDirectories();
            var d = new FilesAndDirectories();
            var u = new FilesAndDirectories();
            FilesAndDirectories.SplitInto(sourceDir, targetDir, a, d, e, u);

            System.IO.File.WriteAllText("deltasync-out-edited.json", JsonConvert.SerializeObject(e, Formatting.Indented));
            System.IO.File.WriteAllText("deltasync-out-deleted.json", JsonConvert.SerializeObject(d, Formatting.Indented));
            System.IO.File.WriteAllText("deltasync-out-added.json", JsonConvert.SerializeObject(a, Formatting.Indented));
            System.IO.File.WriteAllText("deltasync-out-unchanged.json", JsonConvert.SerializeObject(u, Formatting.Indented));

            // write listing
            {
                var fileListing = new StringBuilder();
                foreach (var f in a.Files)
                    fileListing.AppendFormat("{0}{1}\r\n", targetDir.RootFolder, f.Filename);
                foreach (var f in e.Files)
                    fileListing.AppendFormat("{0}{1}\r\n", targetDir.RootFolder, f.Filename);
                System.IO.File.WriteAllText("deltasync-out-files.lst", fileListing.ToString());
            }

            // write removal
            {
                var fileListing = new StringBuilder();
                foreach (var f in d.Files)
                    fileListing.AppendFormat("del /q /f {0}\r\n", f.Filename);
                System.IO.File.WriteAllText("deltasync-out-remove.bat", fileListing.ToString());
            }


        }
    }
}
