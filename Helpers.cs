﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace deltasync
{
    [Serializable]
    public class FileData
    {
        public string Filename;
        public string Hash;
    }


    class FileDataFilenameOnly : IEqualityComparer<FileData>
    {
        public bool Equals(FileData b1, FileData b2)
        {
            if (b1.Filename == b2.Filename)
                return true;
            else
                return false;
        }

        public int GetHashCode(FileData bx)
        {
            return bx.Filename.GetHashCode();
        }
    }

    class FileDataFileAndHash : IEqualityComparer<FileData>
    {
        public bool Equals(FileData b1, FileData b2)
        {
            if (b1.Filename == b2.Filename && b1.Hash == b2.Hash)
                return true;
            else
                return false;
        }

        public int GetHashCode(FileData bx)
        {
            return bx.Filename.GetHashCode() ^ bx.Hash.GetHashCode();
        }
    }

    [Serializable]
    public class FilesAndDirectories
    {
        public string RootFolder;
        public List<string> Directories = new List<string>();
        public List<FileData> Files = new List<FileData>();

        public void CalculateHashes()
        {
            Parallel.ForEach(Files, (x) =>
            {
                if (x.Hash == null)
                {
                    try
                    {
                        x.Hash = Helpers.CalculateMD5(Path.Join(RootFolder, x.Filename));
                    }
                    catch(Exception)
                    {
                        x.Hash = "Unknown-" + System.Guid.NewGuid().ToString();
                    }
                }
            });
        }

        public void CalculateDateTimeAsHash()
        {
            Parallel.ForEach(Files, (x) =>
            {
                if (x.Hash == null)
                {
                    try
                    {
                        x.Hash = System.IO.File.GetLastWriteTime(Path.Join(RootFolder, x.Filename)).ToString("yyyy-MM-dd HH:mm:ss.fff",
                                            System.Globalization.CultureInfo.InvariantCulture);
                    }
                    catch (Exception)
                    {
                        x.Hash = "Unknown-" + System.Guid.NewGuid().ToString();
                    }
                }
            });
        }

        public void Merge(FilesAndDirectories other)
        {
            Directories.AddRange(other.Directories);
            Files.AddRange(other.Files);
        }

        public static void SplitInto(FilesAndDirectories Source, FilesAndDirectories Target, FilesAndDirectories Added, FilesAndDirectories Removed, FilesAndDirectories Edited, FilesAndDirectories Unchanged)
        {
            var eqFilename = new FileDataFilenameOnly();
            if(Added != null)
            { 
                Added.RootFolder = Target.RootFolder;
                Added.Files = Target.Files.Except(Source.Files, eqFilename).ToList();
                Added.Directories = Target.Directories.Except(Source.Directories).ToList();
            }

            var SourceDict = Source.Files.ToDictionary((x) => x.Filename, (x) => x.Hash);

            if (Edited != null)
            {
                Edited.RootFolder = Target.RootFolder;

                Target.Files.ForEach((x) =>
                {
                    if (SourceDict.ContainsKey(x.Filename) && SourceDict[x.Filename] != x.Hash)
                    {
                        Edited.Files.Add(x);
                    }
                });
            }

            if (Unchanged != null)
            {
                Unchanged.RootFolder = Target.RootFolder;

                Target.Files.ForEach((x) =>
                {
                    if (SourceDict.ContainsKey(x.Filename) && SourceDict[x.Filename] == x.Hash)
                    {
                        Unchanged.Files.Add(x);
                    }
                });

                Unchanged.Directories = Target.Directories.Intersect(Source.Directories).ToList();
            }


            if (Removed != null)
            { 
                Removed.RootFolder = Target.RootFolder;
                Removed.Files = Source.Files.Except(Target.Files, eqFilename).ToList();
                Removed.Directories = Source.Directories.Except(Target.Directories).ToList();
            }
        }
    }

    public class Helpers
    {
        public static string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        // Process all files in the directory passed in, recurse on any directories
        // that are found, and process the files they contain.
        public static async Task<FilesAndDirectories> GatherFilesAndDirectories(string startDirectory, string targetDirectory=null)
        {
            FilesAndDirectories fnd = new FilesAndDirectories();
            fnd.RootFolder = startDirectory;
            if (targetDirectory == null)
                targetDirectory = startDirectory;

            var str = targetDirectory.Substring(startDirectory.Length);
            if(str != "") 
                fnd.Directories.Add(str);

            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                fnd.Files.Add(new FileData { Filename = fileName.Substring(startDirectory.Length) });

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            List<Task<FilesAndDirectories>> Subdirs = new List<Task<FilesAndDirectories>>();
            foreach (string subdirectory in subdirectoryEntries)
            {
                var tsk = GatherFilesAndDirectories(startDirectory, subdirectory);
                try { tsk.Start(); } catch (Exception) { }
                Subdirs.Add(tsk);
            }
            // wait for all results
            await Task.WhenAll(Subdirs);

            // merge
            foreach(var subdir in Subdirs)
            {
                fnd.Merge(subdir.Result);
            }
            return fnd;
        }
    }
}
