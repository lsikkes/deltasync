﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace deltasync
{
    [Serializable]
    // Define a class to receive parsed values
    public class Options
    {
        [Option('s', "source", Required = false,
          HelpText = "Input folder")]
        public string SourceFolder { get; set; }

        [Option('t', "target", Required = false,
            HelpText = "Target folder")]
        public string TargetFolder { get; set; }

        [Option('a', "snapshot", Default = false,
  HelpText = "Generate snapshot from source folder")]
        public bool Snapshot { get; set; }

        [Option('d', "date", Default = false,
HelpText = "Use date modified instead of MD5. Less reliable but much faster if you can use it")]
        public bool DateModified { get; set; }

        [Option('v', "verbose", Default = true,
          HelpText = "Prints all messages to standard output.")]
        public bool Verbose { get; set; }
    }
}
